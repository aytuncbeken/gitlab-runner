#!/bin/bash
echo "RUNNER_CONFIG_FILE=${RUNNER_CONFIG_FILE}"
echo "RUNNER_WORKING_DIR=${RUNNER_WORKING_DIR}"
echo "RUNNER_SERVICE=${RUNNER_SERVICE}"
echo "GITLAB_URL=${GITLAB_URL}"
echo "GITLAB_TOKEN=${GITLAB_TOKEN}"
echo "K8N_NAMESPACE=${K8N_NAMESPACE}"
echo "CONCURRENT=${CONCURRENT}"
if [ ! -f ${RUNNER_CONFIG_FILE} ]; then
  echo "${RUNNER_CONFIG_FILE} not exists, registering runner to ${GITLAB_URL}"
  gitlab-runner register --config ${RUNNER_CONFIG_FILE} --non-interactive --registration-token ${GITLAB_TOKEN} --url ${GITLAB_URL} --executor kubernetes --kubernetes-namespace ${K8N_NAMESPACE} --request-concurrency ${CONCURRENT}
fi
echo "Starting runner"
gitlab-runner run -working-directory ${RUNNER_WORKING_DIR} --config ${RUNNER_CONFIG_FILE} --service ${RUNNER_SERVICE} --user gitlab-runner

# trap ctrl-c and call ctrl_c()
trap terminate_handler SIGTERM
trap terminate_handler SIGINT
trap terminate_handler SIGKILL

function terminate_handler() {
  echo "Caught CTRL-C Signal"
  echo "Quitting Gitlab Runner"
  pkill gitlab-runner
}
