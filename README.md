GitLab Runner for OpenShift
==============

Yet another GitLab-Runner - OpenShift implementation. This solution has the ability to register first and then run.
By this way there is no need to register manually in the container or pod.
Build On GitLab Runner image from official repository at docker hub.
https://hub.docker.com/r/gitlab/gitlab-runner/
Inspired from https://raw.githubusercontent.com/oprudkyi/openshift-templates/master/gitlab-runner/README.md

Installation
==============

1. Create new project/namespace and select

	```sh
	oc login -u admin
	oc new-project gitlab-runner
  oc project gitlab-runner
	```

2. Import template

	```sh
	oc create -f https://gitlab.com/aytuncbeken/gitlab-runner/raw/master/openshift/gitlab-runner.yml
	```

3. Setup Security Context Constraints (SCC) for service accounts used for running containers (`anyuid` means commands inside containers can run as root)

	```sh
	oc login -u system:admin
	oc adm policy add-scc-to-user anyuid -z sa-gitlab-runner -n gitlab-runner
	```

4. Go to web console https://MASTER-IP:8443/console/project/gitlab-runner/overview (where MASTER-IP is IP where cluster is bound) and press "Add to Project" and select "gitlab-runner" template

5. Fill required fields
  - GitLab Url: Address of GitLab you are using. ex: https://gitlab.com
  - GitLab Registration Token : You can find it in Project -> Settings -> CI/CD -> Runners
  - Project Name : Type project name which GitLab-Runner will run. If not sure enter project name `gitlab-runner`
  - Image Tag : Tag of Image from https://cloud.docker.com/repository/docker/aytuncbeken/gitlab-runner
  - Concurrent: Number of concurrent jobs

7. After pressing "Create" the deployment will start, it may take few minutes to download required images and preconfigure them

8. In your Gitlab Project check "Runners" page to have runner activated

9. Run some CI job , there will be something like

	```
	Waiting for pod gitlab-runner/runner-86251ae3-project-1142978-concurrent-0uzqax to be running, status is Pending
	```
	in log output of CI

StatefulSets
==============
You can create statefulsets from template defined in the repository. 
```
oc apply -f gitlab-runner-statefulset-template.yml
```
Or you can process template file directly and create statefulsets
```
oc process -f gitlab-runner-statefulset-template.yml -p GITLAB_URL="GITLAB_URL" -p GITLAB_TOKEN="GITLAB_TOKEN" -p NAME_PREFIX="NAME_PREFIX" -p STORAGE_CLASS_NAME="STORAGE_CLASS_NAME" | oc apply -f -
```

